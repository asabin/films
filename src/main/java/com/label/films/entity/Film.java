package com.label.films.entity;

import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author LabeL
 * @version 1.0, 19.02.14
 */

@Entity
@Table(name = "Film")
public class Film {

    private Integer filmId;
    private String filmName;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "film_id", unique = true, nullable = false)
    public Integer getFilmId() {
        return filmId;
    }

    public void setFilmId(Integer filmId) {
        this.filmId = filmId;
    }

    @Column(name = "film_name", unique = true, nullable = true, length = 50)
    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }
}