package com.label.films.dao.impl;

import com.label.films.dao.FilmDao;
import com.label.films.entity.Film;

import java.util.List;

/**
 * @author LabeL
 * @version 1.0, 19.02.14
 */

public class FilmDaoImpl implements FilmDao {

    @Override
    public void addFilm(Film film) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void updateFilm(Film film) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void deleteFilm(Film film) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Film findFilmById(Integer id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<Film> getAllFilms() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}