package com.label.films.dao;

import com.label.films.entity.Film;

import java.util.List;

/**
 * @author LabeL
 * @version 1.0, 19.02.14
 */

public interface FilmDao {

    public void addFilm(Film film);

    public void updateFilm(Film film);

    public void deleteFilm(Film film);

    public Film findFilmById(Integer id);

    public List<Film> getAllFilms();
}